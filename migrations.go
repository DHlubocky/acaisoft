package main

import (
	"acaisoftProject/models/schema"
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func ExecuteMigrations() {
	tx := DbCon.New()
	options := gormigrate.DefaultOptions
	m := gormigrate.New(tx, options, migrationList)
	logger.WithField("NumberOfMigrations", len(migrationList)).Info("Start of Executing AcaiSoft Migrations")
	if err := m.Migrate(); err != nil && err != gormigrate.ErrNoMigrationDefined {
		logger.WithError(err).Fatal("Can not Execute Migrations, something is wrong")
		return
	}
	logger.WithField("NumberOfMigrations", len(migrationList)).Info("Successfully executed Migrations")
}

var migrationList = []*gormigrate.Migration{
	{
		ID: "202104171358-Messages-INIT",
		Migrate: func(tx *gorm.DB) (err error) {
			tx = tx.AutoMigrate(&schema.Messages{})
			err = tx.Error
			return err
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Exec(` DROP TABLE IF EXIST "Messages"`).Error
		},
	},
	{
		ID: "202104180117-Users-INIT",
		Migrate: func(tx *gorm.DB) (err error) {
			tx = tx.AutoMigrate(&schema.Users{})
			err = tx.Error
			return err
		},
		Rollback: func(tx *gorm.DB) error {
			return tx.Exec(` DROP TABLE IF EXIST "Users"`).Error
		},
	},
}
