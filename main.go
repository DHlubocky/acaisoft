package main

import (
	"acaisoftProject/controller"
	"acaisoftProject/repository"
	"acaisoftProject/service"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var logger = logrus.New()
var DbCon *gorm.DB
var viperProvider = viper.New()

var Repositories struct {
	AcaiSoft *repository.AcaiSoft
}
var Services struct {
	AcaiSoft *service.AcaiSoft
}
var Controllers struct {
	AcaiSoft *controller.AcaiSoft
}

func main() {
	ConStopFunc, err := PrepareMicroservice()
	if err != nil {
		logger.WithError(err).Fatalf(`error in Preparing start of Microservice`)
	}
	defer ConStopFunc()
	logger.Info("Preparing MCS Successful")

	ExecuteMigrations()

	ForeverFunc()

}
