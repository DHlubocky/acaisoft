package main

import (
	"acaisoftProject/controller"
	"acaisoftProject/repository"
	"acaisoftProject/repository/storage"
	"acaisoftProject/service"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"os"
)

func ForeverFunc() {
	forever := make(chan bool)
	<-forever
}

func Hello() {
	//Font Name: Big Money-nw
	fmt.Println(`
 $$$$$$\                     $$\  $$$$$$\             $$$$$$\    $$\                                             
$$  __$$\                    \__|$$  __$$\           $$  __$$\   $$ |                                            
$$ /  $$ | $$$$$$$\ $$$$$$\  $$\ $$ /  \__| $$$$$$\  $$ /  \__|$$$$$$\         $$$$$$\$$$$\   $$$$$$$\  $$$$$$$\ 
$$$$$$$$ |$$  _____|\____$$\ $$ |\$$$$$$\  $$  __$$\ $$$$\     \_$$  _|        $$  _$$  _$$\ $$  _____|$$  _____|
$$  __$$ |$$ /      $$$$$$$ |$$ | \____$$\ $$ /  $$ |$$  _|      $$ |          $$ / $$ / $$ |$$ /      \$$$$$$\  
$$ |  $$ |$$ |     $$  __$$ |$$ |$$\   $$ |$$ |  $$ |$$ |        $$ |$$\       $$ | $$ | $$ |$$ |       \____$$\ 
$$ |  $$ |\$$$$$$$\\$$$$$$$ |$$ |\$$$$$$  |\$$$$$$  |$$ |        \$$$$  |      $$ | $$ | $$ |\$$$$$$$\ $$$$$$$  |
\__|  \__| \_______|\_______|\__| \______/  \______/ \__|         \____/       \__| \__| \__| \_______|\_______/`)

}

func PrepareMicroservice() (stopFunc func(), err error) {
	Hello()
	err = PrepareViper()
	if err != nil {
		return
	}
	err = PrepareLogger()
	if err != nil {
		return
	}
	err = PreparePostgresDb()
	if err != nil {
		return
	}
	err = PrepareRepositories()
	if err != nil {
		return
	}
	err = PrepareServices()
	if err != nil {
		return
	}
	err = PrepareControllers()
	if err != nil {
		return
	}
	return nil, nil
}

func PrepareViper() (err error) {
	envKey := "GOLANG_ENVIROMENT"
	enviromentString := ""
	if s := os.Getenv(envKey); len(s) > 0 {
		enviromentString = "." + s
	}

	viperProvider.SetConfigName("appsettings" + enviromentString)
	viperProvider.AddConfigPath(".")
	err = viperProvider.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		return err
	}
	viperProvider.SetDefault("Logger.level", logrus.InfoLevel.String())

	return nil

}

func PrepareLogger() (err error) {
	temp := viperProvider.GetString("Logger.level")
	level, err := logrus.ParseLevel(temp)
	if err != nil {
		level = logrus.DebugLevel
		logger.WithField("error", err).Warning("Error in parsing level string for Logger, level set ='debug'")
	}
	logger.SetLevel(level)

	switch viperProvider.GetString("Logger.formatter.name") {
	case "json":
		logger.Formatter = &logrus.JSONFormatter{}
		break
	case "text":
		logger.Formatter = &logrus.TextFormatter{
			ForceColors: true,
		}
		break
	default:
		logger.Formatter = &logrus.TextFormatter{
			ForceColors: true,
		}
	}

	return nil

}

func PreparePostgresDb() (err error) {
	settings := &storage.PostgresDbSettings{}
	err = viperProvider.UnmarshalKey("PostgresDb", settings)
	if err != nil {
		return
	}
	DbCon, err = gorm.Open(settings.Dialect(), settings.OpenString())
	//DbCon, err = nil, nil
	if err != nil {
		return
	}
	DbCon.SetLogger(logger)
	DbCon.LogMode(settings.LogMode)
	return
}

//Place for Initializing all Repositories
func PrepareRepositories() (err error) {
	if Repositories.AcaiSoft, err = repository.NewAcaisoft("Repositories.AcaiSoft", viperProvider, logger, DbCon); err != nil {
		return
	}
	return
}

//Place for Initializing all Services
func PrepareServices() (err error) {
	if Services.AcaiSoft, err = service.NewAcaisoft("Services.AcaiSoft", viperProvider, logger, Repositories.AcaiSoft); err != nil {
		return
	}
	return
}

//Place for Initializing all Controllers
func PrepareControllers() (err error) {
	if Controllers.AcaiSoft, err = controller.NewAcaisoft("Controllers.AcaiSoft", viperProvider, logger, Services.AcaiSoft); err != nil {
		return
	}
	if err = Controllers.AcaiSoft.RequestHandler(); err != nil {
		return
	}
	return
}
