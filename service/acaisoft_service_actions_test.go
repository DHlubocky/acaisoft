package service

import (
	event "acaisoftProject/models/events"
	"acaisoftProject/repository"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

func TestAcaiSoft_CreateMessage(t *testing.T) {
	type fields struct {
		logger *logrus.Logger
		Repo   *repository.AcaiSoft
	}
	type args struct {
		request *event.AddMessageRequest
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantResponse *event.AddMessageResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &AcaiSoft{
				logger: tt.fields.logger,
				Repo:   tt.fields.Repo,
			}
			gotResponse, err := s.CreateMessage(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("CreateMessage() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func TestAcaiSoft_DeleteMessage(t *testing.T) {
	type fields struct {
		logger *logrus.Logger
		Repo   *repository.AcaiSoft
	}
	type args struct {
		request *event.DeleteMessageRequest
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantResponse *event.DeleteMessageResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &AcaiSoft{
				logger: tt.fields.logger,
				Repo:   tt.fields.Repo,
			}
			gotResponse, err := s.DeleteMessage(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("DeleteMessage() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func TestAcaiSoft_IndexMessage(t *testing.T) {
	type fields struct {
		logger *logrus.Logger
		Repo   *repository.AcaiSoft
	}
	type args struct {
		request *event.IndexMessagesRequest
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantResponse *event.IndexMessagesResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &AcaiSoft{
				logger: tt.fields.logger,
				Repo:   tt.fields.Repo,
			}
			gotResponse, err := s.IndexMessage(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("IndexMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("IndexMessage() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}

func TestAcaiSoft_Login(t *testing.T) {
	type fields struct {
		logger *logrus.Logger
		Repo   *repository.AcaiSoft
	}
	type args struct {
		request *event.LoginRequest
	}
	tests := []struct {
		name         string
		fields       fields
		args         args
		wantResponse *event.LoginResponse
		wantErr      bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &AcaiSoft{
				logger: tt.fields.logger,
				Repo:   tt.fields.Repo,
			}
			gotResponse, err := s.Login(tt.args.request)
			if (err != nil) != tt.wantErr {
				t.Errorf("Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResponse, tt.wantResponse) {
				t.Errorf("Login() gotResponse = %v, want %v", gotResponse, tt.wantResponse)
			}
		})
	}
}
