# Acaisoft repository

The service package is responsible for the business logic, and for performing operations on the database, such as adding
rows, deleting them, and fetching them from the database.

## Structure

**acaisoft_service_actions.go**

This file contains methods that include business logic and execute operations on the database.

**acaisoft_service_core.go**

This file contains settings and init methods used in the initialization of the service package.