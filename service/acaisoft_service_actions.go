package service

import (
	event "acaisoftProject/models/events"
	"acaisoftProject/models/schema"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

//This function Creates a new message row in the database using the request object. It returns a success bool and a uuid code
func (s *AcaiSoft) CreateMessage(request *event.AddMessageRequest) (response *event.AddMessageResponse, err error) {
	var (
		message *schema.Messages
	)
	response = new(event.AddMessageResponse)
	message = new(schema.Messages)
	message.Email = request.Email
	message.Content = request.Content
	message.Title = request.Title
	uuidString, err := uuid.NewV1()
	if err != nil {
		return
	}
	message.Code = uuidString.String()
	err = s.Repo.Create(message)
	if err != nil {
		return
	}
	response.Code = message.Code
	response.Completed = true
	return
}

//This function Deletes a message row in the database using the request object. It returns a success bool. Note that in this case a "soft" delete is preferred, keeping the record in the DB, for rollback purposes.
func (s *AcaiSoft) DeleteMessage(request *event.DeleteMessageRequest) (response *event.DeleteMessageResponse, err error) {
	var (
		message *schema.Messages
		filter  schema.MessageFilter
	)
	message = new(schema.Messages)
	filter = schema.MessageFilter{Code: request.Code, Id: request.Id}
	response = new(event.DeleteMessageResponse)
	err = s.Repo.Delete(&filter, &message)
	if err != nil {
		return
	}
	response.Completed = true
	return
}

//This function Indexes message rows in the database, it returns an object array of these rows wrapped in a Node json object.
func (s *AcaiSoft) IndexMessage(request *event.IndexMessagesRequest) (response *event.IndexMessagesResponse, err error) {
	var (
		message []*schema.Messages
		filter  schema.MessageIndexFilter
	)
	response = new(event.IndexMessagesResponse)
	filter = schema.MessageIndexFilter{Email: request.Email}
	err = s.Repo.Index(&filter, &message)
	if err != nil {
		return
	}
	response.Node = message
	return
}

//This function gets user creds from the request and compares them with the users in the database, returning an auth token if it is successful.
func (s *AcaiSoft) Login(request *event.LoginRequest) (response *event.LoginResponse, err error) {
	var (
		user      *schema.Users
		filter    schema.UserFilter
		tokentext string
	)
	user = new(schema.Users)
	response = new(event.LoginResponse)
	filter = schema.UserFilter{
		Username: request.Username,
	}
	err = s.Repo.Read(&filter, &user)
	if err != nil {
		return
	}
	if user.Password != request.Password {
		return nil, errors.Errorf("Wrong password")
	}
	tokentext, err = s.Repo.CreateToken(uint64(user.ID))
	if err != nil {
		return
	}
	response.AuthToken = tokentext
	return
}
