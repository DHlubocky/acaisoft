package service

import (
	"acaisoftProject/repository"
	"acaisoftProject/repository/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type AcaiSoft struct {
	logger *logrus.Logger
	Repo   *repository.AcaiSoft
}

func NewAcaisoft(viperPath string, viperProvider *viper.Viper, logger *logrus.Logger, acaiSoftRepo *repository.AcaiSoft) (ns *AcaiSoft, err error) {
	ns = new(AcaiSoft)
	var (
		packageName = "service"
		structName  = "AcaiSoft"
		funcName    = "NewAcaiSoft"
	)
	switch {
	case len(viperPath) == 0:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperPath(string)")
	case viperProvider == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperProvider(*viper.Viper)")
	case logger == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " logger(*logrus.Logger)")
	case acaiSoftRepo == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " acaiSoftRepo(*repository.acaiSoft)")
	}
	if err != nil {
		return
	}
	ns.logger = logger
	ns.Repo, err = acaiSoftRepo.New()
	if err != nil {
		return
	}

	return

}
func (s *AcaiSoft) New() (ns *AcaiSoft, err error) {
	var (
		packageName = "service"
		structName  = "AcaiSoft"
		funcName    = "New"
	)
	ns = new(AcaiSoft)
	switch {
	case s == nil:
		err = locerrors.NewNilMethodReceiver(packageName, structName, funcName)
	}
	if err != nil {
		return
	}
	if ns.Repo, err = s.Repo.New(); err != nil {
		return
	}

	return
}
