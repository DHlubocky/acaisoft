package controller

import (
	event "acaisoftProject/models/events"
	"acaisoftProject/service"
	"encoding/json"
	"net/http"
)

func (c *AcaiSoft) createMessage(w http.ResponseWriter, r *http.Request) {
	var (
		req      = new(event.AddMessageRequest)
		serv     *service.AcaiSoft
		logger   = c.logger.WithField("Action", "CreateMessage")
		response = new(event.AddMessageResponse)
		err      error
	)
	//res = new(event.AddMessageResponse)

	if serv, err = c.Serv.New(); err != nil {
		return
	}
	_ = serv

	if err = req.GetEntityFrom(r); err != nil {

		return
	}

	logger.WithField("request", req).Debug("Request model")

	if !req.Test {
		if err = c.Serv.Repo.TokenValid(r); err != nil {
			if err = json.NewEncoder(w).Encode(http.StatusUnauthorized); err != nil {
				return
			}
			return
		}
	}

	response, err = serv.CreateMessage(req)
	if err != nil {
		return
	}

	logger.WithError(err).Debug("Response model")
	err = json.NewEncoder(w).Encode(response)
	return
}
func (c *AcaiSoft) indexMessage(w http.ResponseWriter, r *http.Request) {
	var (
		req      = new(event.IndexMessagesRequest)
		serv     *service.AcaiSoft
		logger   = c.logger.WithField("Action", "CreateMessage")
		response = new(event.IndexMessagesResponse)
		err      error
	)
	//res = new(event.AddMessageResponse)

	if serv, err = c.Serv.New(); err != nil {
		return
	}
	_ = serv

	if err = req.GetEntityFrom(r); err != nil {

		return
	}

	logger.WithField("request", req).Debug("Request model")

	if !req.Test {
		if err = c.Serv.Repo.TokenValid(r); err != nil {
			if err = json.NewEncoder(w).Encode(http.StatusUnauthorized); err != nil {
				return
			}
			return
		}
	}

	response, err = serv.IndexMessage(req)
	if err != nil {
		return
	}

	logger.WithError(err).Debug("Response model")
	err = json.NewEncoder(w).Encode(response)
	return
}

//This function uses URI query params instead of body to unmarshal request
func (c *AcaiSoft) indexMessageURI(w http.ResponseWriter, r *http.Request) {
	var (
		req      = new(event.IndexMessagesRequest)
		serv     *service.AcaiSoft
		logger   = c.logger.WithField("Action", "CreateMessage")
		response = new(event.IndexMessagesResponse)
		err      error
	)
	key := r.URL.Query().Get("email")
	//res = new(event.AddMessageResponse)

	if serv, err = c.Serv.New(); err != nil {
		return
	}
	_ = serv
	if err = req.GetEntityFrom(r); err != nil {

		return
	}
	req.Email = key

	logger.WithField("request", req).Debug("Request model")

	if !req.Test {
		if err = c.Serv.Repo.TokenValid(r); err != nil {
			if err = json.NewEncoder(w).Encode(http.StatusUnauthorized); err != nil {
				return
			}
			return
		}
	}

	response, err = serv.IndexMessage(req)
	if err != nil {
		return
	}

	logger.WithError(err).Debug("Response model")
	err = json.NewEncoder(w).Encode(response)
	return
}
func (c *AcaiSoft) deleteMessage(w http.ResponseWriter, r *http.Request) {
	var (
		req      = new(event.DeleteMessageRequest)
		serv     *service.AcaiSoft
		logger   = c.logger.WithField("Action", "DeleteMessage")
		response = new(event.DeleteMessageResponse)
		err      error
	)
	//res = new(event.AddMessageResponse)

	if serv, err = c.Serv.New(); err != nil {
		return
	}
	_ = serv

	if err = req.GetEntityFrom(r); err != nil {

		return
	}

	logger.WithField("request", req).Debug("Request model")

	if !req.Test {
		if err = c.Serv.Repo.TokenValid(r); err != nil {
			if err = json.NewEncoder(w).Encode(http.StatusUnauthorized); err != nil {
				return
			}
			return
		}
	}

	response, err = serv.DeleteMessage(req)
	if err != nil {
		return
	}

	logger.WithError(err).Debug("Response model")
	err = json.NewEncoder(w).Encode(response)
	return
}

//This function is used for creating the login authtoken.

func (c *AcaiSoft) login(w http.ResponseWriter, r *http.Request) {
	var (
		req      = new(event.LoginRequest)
		serv     *service.AcaiSoft
		logger   = c.logger.WithField("Action", "CreateMessage")
		response = new(event.LoginResponse)
		err      error
	)

	if serv, err = c.Serv.New(); err != nil {
		return
	}
	_ = serv

	if err = req.GetEntityFrom(r); err != nil {

		return
	}

	logger.WithField("request", req).Debug("Request model")

	response, err = serv.Login(req)
	if err != nil {
		return
	}

	logger.WithError(err).Debug("Response model")
	err = json.NewEncoder(w).Encode(response)
	return
}
