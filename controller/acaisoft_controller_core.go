package controller

import (
	locerrors "acaisoftProject/repository/errors"
	"acaisoftProject/service"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"log"
	"net/http"
	"time"
)

type AcaiSoft struct {
	logger   *logrus.Logger
	Serv     *service.AcaiSoft
	Settings *AcaiSoftSettings
}
type AcaiSoftSettings struct {
	Address string `json:"address"`
}

//Definitions and init for the methods enum
type Methods int

const (
	Unknown Methods = iota
	POST
	GET
	DELETE
)

//String overload for methods enum
func (enum Methods) String() string {
	names := []string{
		"Unknown",
		"POST",
		"GET",
		"DELETE",
	}
	if enum < Unknown && enum > DELETE {
		return Unknown.String()
	}

	return names[enum]
}

//This is the init function for the controller package
func NewAcaisoft(viperPath string, viperProvider *viper.Viper, logger *logrus.Logger, acaiSoftServ *service.AcaiSoft) (nc *AcaiSoft, err error) {
	nc = new(AcaiSoft)
	var (
		packageName = "controller"
		structName  = "AcaiSoft"
		funcName    = "NewAcaiSoft"
	)
	switch {
	case len(viperPath) == 0:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperPath(string)")
	case viperProvider == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperProvider(*viper.Viper)")
	case logger == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " logger(*logrus.Logger)")
	case acaiSoftServ == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " acaiSoftServ(*service.acaiSoft)")
	}
	if err != nil {
		return
	}
	nc.logger = logger
	nc.Serv, err = acaiSoftServ.New()
	if err != nil {
		return
	}
	//This function unmarshals the appsettings Controller settings into the settings struct
	if err = viperProvider.UnmarshalKey(viperPath, &nc.Settings); err != nil {
		return
	}

	return

}

//This function prepares the web server for routing and also prepares endpoints
func (c *AcaiSoft) RequestHandler() (err error) {
	var (
		//Here you define the endpoints, putting them in a variable reduces typing down the line
		messageEP    = "/api/message"
		loginEP      = "/api/login"
		messageURIEP = "/api/message/"
	)
	//Here you init and define the web server, notice that the address is pulled from appsettings, where you can change it if you wish. Also notice timeouts set to prevent slowloris ddos attacks
	r := mux.NewRouter().StrictSlash(true)
	srv := &http.Server{
		Addr: c.Settings.Address,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}
	//Here you define the actual handler functions, assigning them to an endpoint and giving them a specific method using the enums defined above. The receiver methods are defined in acaisoft_controller_actions.go, defined on the controller package.
	err = r.HandleFunc(messageEP, c.createMessage).Methods(POST.String()).GetError()
	if err != nil {
		return
	} else {
		c.logger.WithField("Address", srv.Addr+messageEP).WithField("Method", POST.String()).Infof("Endpoint prepared to receive requests")
	}
	//Here the function is different, using the Queries method to extract query params from the url instead of the body. In this case it extracts the email param, and uses regex to match it to any string
	err = r.HandleFunc(messageURIEP, c.indexMessageURI).Methods(GET.String()).Queries("email", "{.*}").GetError()
	if err != nil {
		return
	} else {
		c.logger.WithField("Address", srv.Addr+messageURIEP+"?email={email}").WithField("Method", GET.String()).Infof("Endpoint prepared to receive requests")
	}
	err = r.HandleFunc(messageEP, c.indexMessage).Methods(GET.String()).GetError()
	if err != nil {
		return
	} else {
		c.logger.WithField("Address", srv.Addr+messageEP).WithField("Method", GET.String()).Infof("Endpoint prepared to receive requests")
	}
	err = r.HandleFunc(messageEP, c.deleteMessage).Methods(DELETE.String()).GetError()
	if err != nil {
		return
	} else {
		c.logger.WithField("Address", srv.Addr+messageEP).WithField("Method", DELETE.String()).Infof("Endpoint prepared to receive requests")
	}
	err = r.HandleFunc(loginEP, c.login).Methods(POST.String()).GetError()
	if err != nil {
		return
	} else {
		c.logger.WithField("Address", srv.Addr+loginEP).WithField("Method", POST.String()).Infof("Endpoint prepared to receive requests")
	}
	//Here a goroutine is used, putting the server on a separate thread, therefore preventing the main thread from blocking the migrations and other such necessary processes. This could be moved to the main package, but this is preferred for convention consolidation
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	return
}
