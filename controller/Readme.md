# Acaisoft controller

The controller is used for unmarshalling and handling requests and responses, this package should not handle business
logic.

## Structure

**acaisoft_controller_core.go**

The purpose of this file is to store init functions, settings, and the web server/request handler for the acaisoft
controller

**acaisoft_controller_actions.go**

The purpose of this file is to store functions responsible for unmarshalling requests, authenticating requests (given
that the test param is false) and for marshalling responses. 