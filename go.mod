module acaisoftProject

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.2
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	gopkg.in/gormigrate.v1 v1.6.0
)
