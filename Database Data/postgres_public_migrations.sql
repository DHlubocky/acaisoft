create table migrations
(
    id varchar(255) not null
        constraint migrations_pkey
            primary key
);

alter table migrations
    owner to postgres;

INSERT INTO public.migrations (id)
VALUES ('202104171358-Messages-INIT');
INSERT INTO public.migrations (id)
VALUES ('202104180117-Users-INIT');