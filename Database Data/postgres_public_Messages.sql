create table "Messages"
(
    id         serial not null
        constraint "Messages_pkey"
            primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    "Code"     text,
    "Title"    text,
    "Email"    text,
    "Content"  text
);

alter table "Messages"
    owner to postgres;

create
index idx_messages_deleted_at
    on "Messages" (deleted_at);

INSERT INTO public."Messages" (id, created_at, updated_at, deleted_at, "Code", "Title", "Email", "Content")
VALUES (1, '2021-04-18 00:05:07.276220', '2021-04-18 00:05:07.276220', null, 'bad2ec50-9fd9-11eb-9aeb-4ccc6a7e6371',
        'Interview', 'jan.kowalski@example.com', 'simple text');
INSERT INTO public."Messages" (id, created_at, updated_at, deleted_at, "Code", "Title", "Email", "Content")
VALUES (2, '2021-04-19 19:07:48.617289', '2021-04-19 19:07:48.617289', null, '873a2183-a142-11eb-a238-4ccc6a7e6371',
        'Interview 2', 'jan.kowalski@example.com', 'simple text 2');