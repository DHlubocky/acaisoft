create table "Users"
(
    id         serial not null
        constraint "Users_pkey"
            primary key,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone,
    "Code"     text,
    "Name"     text,
    "Username" text,
    "Password" text
);

alter table "Users"
    owner to postgres;

create
index idx_users_deleted_at
    on "Users" (deleted_at);

INSERT INTO public."Users" (id, created_at, updated_at, deleted_at, "Code", "Name", "Username", "Password")
VALUES (1, null, null, null, null, 'Test User', 'demo', '1234');