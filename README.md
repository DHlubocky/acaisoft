# AcaiSoft

Project for acaisoft interview.

## Requests

### CreateMessage

##### Url: `URL:PORT/api/message`

##### Method: `POST`

##### Description:

This request creates a new message in the DB

##### Headers:

```json
{
  "Authorization": "string"
}
```

Authorization stores jwt token unless test bool is true

##### Request Model:

```json
{
  "email": "string",
  "content": "string",
  "title": "string",
  "test": true
}
```

Test bool skips authorization if true

##### Response Model:

```json
{
  "Completed": true,
  "Code": "string"
}
```

### IndexMessage

##### Url: `URL:PORT/api/message`

##### Method: `GET`

##### Description:

This request indexes messages from the DB by email from body

##### Headers:

```json
{
  "Authorization": "string"
}
```

Authorization stores jwt token unless test bool is true

##### Request Model:

```json
{
  "email": "string",
  "test": true
}
```

Test bool skips authorization if true

##### Response Model:

```json
{
  "Node": [{}]
}
```

### IndexMessageURI

##### Url: `URL:PORT/api/message/?email={email}`

##### Method: `GET`

##### Description:

This request indexes messages from the DB by email from URI

##### Headers:

```json
{
  "Authorization": "string"
}
```

Authorization stores jwt token unless test bool is true

##### Request Model:

```json
{
  "test": true
}
```

Test bool skips authorization if true

##### Response Model:

```json
{
  "Node": [{}]
}
```

### DeleteMessage

##### Url: `URL:PORT/api/message`

##### Method: `DELETE`

##### Description:

This request soft deletes a message in the database

##### Headers:

```json
{
  "Authorization": "string"
}
```

Authorization stores jwt token unless test bool is true

##### Request Model:

```json
{
  "id": 1,
  "code": "string",
  "test": true
}
```

Test bool skips authorization if true

##### Response Model:

```json
{
  "Completed": true
}
```

### Login

##### Url: `URL:PORT/api/login`

##### Method: `POST`

##### Description:

This request generates an AuthToken if username and password match one found in the db. NOTE: Usually the password in
the db and the one sent in the request would be hashed, this is not so in this case for testing purposes

##### Request Model:

```json
{
  "username": "string",
  "password": "string"
}
```

Test bool skips authorization if true

##### Response Model:

```json
{
  "auth_token": "string"
}
```