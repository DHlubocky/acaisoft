# Acaisoft repository

The repository package is responsible for operations persistent operations, this includes the database, and the creation
of login jwt tokens, as well as their validation

## Structure

**locerrors**

This sub package is responsible for commonly used errors

**storage**

This sub package is responsible for persistent database actions and settings, including base filters, surface actions
for database operations like read and index and so on

**acaisoft_repository_actions.go**

This file contains methods used in repository based operations, like auth token creation and validation

**acaisoft_repository_core.go**

This file contains settings and init methods used in the initialization of the repository package, and the gorm package. 