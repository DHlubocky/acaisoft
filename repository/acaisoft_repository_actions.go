package repository

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
	"time"
)

//This function extracts the auth token from the request headers
func (s *AcaiSoft) ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 1 {
		return strArr[0]
	}
	return ""
}

//This function verifies the token against the signing key found in appsettings
func (s *AcaiSoft) VerifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := s.ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(s.RepoSettings.SigningKey), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

//This function creates an auth token using a signing key found in appsettings.json and the user id of the user
func (s *AcaiSoft) CreateToken(userid uint64) (string, error) {
	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userid
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(s.RepoSettings.SigningKey))
	if err != nil {
		return "", err
	}
	return token, nil
}

//This is a front facing function that does the conditional operation on the token
func (s *AcaiSoft) TokenValid(r *http.Request) error {
	token, err := s.VerifyToken(r)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}
