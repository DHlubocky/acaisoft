package locerrors

import "fmt"

//This errors is called when a function receives an empty or nil parameter, causing it to fail.
type FunctionParamEmptyErr struct {
	Package    string
	StructName string
	Method     string
	Param      string
}

func NewFunctionParamEmptyErr(packageName, structName, methodName, parameterName string) (err *FunctionParamEmptyErr) {
	err = new(FunctionParamEmptyErr)
	err.Package = packageName
	err.StructName = structName
	err.Method = methodName
	err.Param = parameterName
	return
}

func (e FunctionParamEmptyErr) Error() string {
	return fmt.Sprintf(`empty input parameter: %s, from function: %s, for struct: %s, inside of package: %s`, e.Param, e.Method, e.StructName, e.Package)
}
