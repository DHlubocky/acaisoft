package locerrors

import "fmt"

//This errors is called when a receiver method is nil
type NilMethodReceiverErr struct {
	Package    string
	StructName string
	Method     string
}

func NewNilMethodReceiver(packageName, structName, methodName string) (err *NilMethodReceiverErr) {
	err = new(NilMethodReceiverErr)
	err.Package = packageName
	err.StructName = structName
	err.Method = methodName
	return
}
func (e NilMethodReceiverErr) Error() string {
	return fmt.Sprintf(`nil method reciever: %s, for struct: %s, inside of package: %s`, e.Method, e.StructName, e.Package)
}
