package repository

import (
	"acaisoftProject/repository/errors"
	"acaisoftProject/repository/storage"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type AcaiSoft struct {
	logger *logrus.Logger
	storage.GormRepo
	RepoSettings *Settings
}

type Settings struct {
	SigningKey string `json:"SigningKey"`
}

func NewAcaisoft(viperPath string, viperProvider *viper.Viper, logger *logrus.Logger, dbCon *gorm.DB) (ns *AcaiSoft, err error) {
	ns = new(AcaiSoft)
	var (
		packageName = "repository"
		structName  = "AcaiSoft"
		funcName    = "NewAcaiSoft"
	)
	switch {
	case len(viperPath) == 0:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperPath(string)")
	case viperProvider == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " viperProvider(*viper.Viper)")
	case logger == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " logger(*logrus.Logger)")
	case dbCon == nil:
		err = locerrors.NewFunctionParamEmptyErr(packageName, structName, funcName, " dbCon(*gorm.DB)")
	}
	if err != nil {
		return
	}
	err = viperProvider.UnmarshalKey(viperPath, &ns.RepoSettings)
	if err != nil {
		return
	}
	ns.logger = logger
	if ns.GormRepo, err = storage.NewBaseGormRepository(dbCon); err != nil {
		return
	}
	return

}

func (s *AcaiSoft) New() (ns *AcaiSoft, err error) {
	var (
		packageName = "repository"
		structName  = "AcaiSoft"
		funcName    = "New"
	)
	ns = new(AcaiSoft)
	switch {
	case s == nil:
		err = locerrors.NewNilMethodReceiver(packageName, structName, funcName)
	}
	if err != nil {
		return
	}
	if ns.GormRepo, err = s.GormRepo.New(); err != nil {
		return
	}
	ns.RepoSettings = s.RepoSettings
	return
}
