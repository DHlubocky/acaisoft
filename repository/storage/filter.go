package storage

import "github.com/jinzhu/gorm"

type Filter interface {
	Init()
	Apply(dbIn *gorm.DB) (dbOut *gorm.DB, err error)
}

type IndexFilter interface {
	Filter
	GetLimit() int
}
