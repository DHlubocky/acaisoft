package storage

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type BaseIndexFilter struct {
	Offset    int
	Limit     int
	PreLoad   bool
	OrderBy   []string
	HasNext   bool
	TableName string
	quotes    bool
	init      bool
}

//Function for init Filter
func (bif *BaseIndexFilter) InitBase(quotes bool) {
	bif.quotes = quotes
	bif.init = true
}

func (bif *BaseIndexFilter) QueryString(column, action string) string {
	var format string
	if bif.quotes {
		format = `( "%s" %s (?))`
	} else {
		format = `( %s %s (?))`
	}
	return fmt.Sprintf(format, column, action)
}

func (bif *BaseIndexFilter) GetTableName() string {
	return bif.TableName
}

func (bif *BaseIndexFilter) ApplyBase(dbIn *gorm.DB) (dbOut *gorm.DB, applied bool, err error) {
	if dbIn == nil {
		err = errors.Errorf("Database connection is nil")
		return
	} else {
		dbOut = dbIn
	}

	if !bif.init {
		err = errors.Errorf("Filter is not initialized")
		return
	}

	if bif.Offset > 0 {
		dbOut = dbOut.Offset(bif.Offset)
	}
	if bif.Limit > 0 {
		limit := bif.Limit
		if bif.HasNext {
			limit++
		}
		if bif.Limit > 0 {
			dbOut.Limit(bif.Limit)
		}
	}

	return
}

func (bif *BaseIndexFilter) PreloadOn() {
	bif.PreLoad = true
}

func (bif *BaseIndexFilter) PreloadOff() {
	bif.PreLoad = false
}

func (bif *BaseIndexFilter) HasNextOn() {
	bif.HasNext = true
}

func (bif *BaseIndexFilter) HasNextOff() {
	bif.HasNext = false
}
