package storage

import (
	locerrors "acaisoftProject/repository/errors"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type GormRepo struct {
	DbCon                 *gorm.DB
	AssociationAutoupdate bool
}

func NewBaseGormRepository(dbCon *gorm.DB) (nbr GormRepo, err error) {
	nbr.DbCon = dbCon.New()

	return
}
func (br *GormRepo) New() (nbs GormRepo, err error) {
	if br == nil {
		err = locerrors.NewNilMethodReceiver("basetypes", "BaseGormRepository", "New")
		return
	}
	nbs, err = NewBaseGormRepository(br.DbCon)
	return
}
func (br *GormRepo) FreshDbCon() (dbCon *gorm.DB, err error) {

	for attempt := 0; attempt < 5; attempt++ {
		dbCon = br.DbCon.New()
		err = dbCon.DB().Ping()
		if err == nil {
			break
		}
	}
	return
}
func (br *GormRepo) BeginFreshTransaction() (dbCon *gorm.DB, err error) {
	dbCon, err = br.FreshDbCon()
	if err != nil {
		return nil, errors.Wrap(err, "in creating fresh Db connection")
	}
	dbCon = dbCon.Begin()
	return
}
func (br *GormRepo) SetAssociationAutoupdate(enable bool) {
	if br == nil {
		return
	}

	br.AssociationAutoupdate = enable

}
func (br *GormRepo) Create(value interface{}) (err error) {
	var tx *gorm.DB
	if tx, err = br.FreshDbCon(); err != nil {
		return
	}

	tx = tx.Set("gorm:association_autoupdate", br.AssociationAutoupdate)

	tx = tx.Create(value)
	if tx.Error != nil {
		err = tx.Error
		return
	}

	return
}
func (br *GormRepo) Update(filter Filter, value interface{}) (err error) {
	var tx *gorm.DB
	if tx, err = br.FreshDbCon(); err != nil {
		return
	}
	filter.Init()

	tx, err = filter.Apply(tx)
	if err != nil {
		return
	}

	tx = tx.Set("gorm:association_autoupdate", br.AssociationAutoupdate)

	tx = tx.Model(value).Update(value)
	if tx.Error != nil {
		err = tx.Error
		return
	}

	return
}
func (br *GormRepo) Read(filter Filter, value interface{}) (err error) {
	var tx *gorm.DB
	if tx, err = br.FreshDbCon(); err != nil {
		return
	}
	filter.Init()

	tx, err = filter.Apply(tx)
	if err != nil {
		return
	}

	tx = tx.First(value)
	if tx.Error != nil {
		err = tx.Error
		return
	}

	return
}
func (br *GormRepo) Index(filter IndexFilter, value interface{}) (err error) {
	var tx *gorm.DB
	if tx, err = br.FreshDbCon(); err != nil {
		return
	}
	filter.Init()

	tx, err = filter.Apply(tx)
	if err != nil {
		return
	}

	tx = tx.Find(value)
	if tx.Error != nil {
		err = tx.Error
		return
	}

	return
}
func (br *GormRepo) Delete(filter Filter, value interface{}) (err error) {
	var tx *gorm.DB
	if tx, err = br.FreshDbCon(); err != nil {
		return
	}
	filter.Init()

	tx, err = filter.Apply(tx)
	if err != nil {
		return
	}

	tx = tx.Set("gorm:association_autoupdate", br.AssociationAutoupdate)

	tx = tx.Delete(value)
	if tx.Error != nil {
		err = tx.Error
		return
	}

	return
}
