package storage

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type BaseFilter struct {
	quotes bool
	init   bool
}

func (bf *BaseFilter) InitBase(quotes bool) {
	bf.quotes = quotes
	bf.init = true
}
func (bf *BaseFilter) ApplyBase(dbIn *gorm.DB) (dbOut *gorm.DB, err error) {
	if dbIn == nil {
		err = errors.Errorf("Database connection is nil")
		return
	} else {
		dbOut = dbIn
	}
	if !bf.init {
		err = errors.Errorf("Filter is not initialized")
		return
	}

	return
}

func (bf *BaseFilter) QueryString(column, action string) string {
	var format string
	if bf.quotes {
		format = `( "%s" %s (?))`
	} else {
		format = `( %s %s (?))`
	}
	return fmt.Sprintf(format, column, action)
}
