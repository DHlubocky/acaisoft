package storage

import "fmt"

const mcsPostgresDialect = "postgres"
const mcsPostgresStringFormat = "host=%s port=%d user=%s dbname=%s password=%s sslmode=disable"

type PostgresDbSettings struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
	LogMode  bool
}

func (set *PostgresDbSettings) OpenString() string {
	if len(set.Host) == 0 {
		set.Host = "localhost"
	}
	if set.Port == 0 {
		set.Port = 5432
	}
	return fmt.Sprintf(mcsPostgresStringFormat, set.Host, set.Port, set.User, set.DbName, set.Password)
}

func (set *PostgresDbSettings) Dialect() string {
	return mcsPostgresDialect
}
