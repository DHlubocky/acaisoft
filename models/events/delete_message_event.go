package event

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type DeleteMessageRequest struct {
	Id   int    `json:"id"`
	Code string `json:"code"`
	Test bool   `json:"test"`
}

func (e *DeleteMessageRequest) GetEntityFrom(d *http.Request) error {
	if err := e.UnMarshallBody(d); err != nil {
		return err
	}
	return nil
}
func (e *DeleteMessageRequest) UnMarshallBody(d *http.Request) error {
	reqBody, _ := ioutil.ReadAll(d.Body)
	return json.Unmarshal(reqBody, e)
}

func (e *DeleteMessageRequest) MarshalIntoBody() ([]byte, error) {
	return json.Marshal(e)
}

type DeleteMessageResponse struct {
	Completed bool
}
