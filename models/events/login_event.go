package event

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (e *LoginRequest) GetEntityFrom(d *http.Request) error {
	if err := e.UnMarshallBody(d); err != nil {
		return err
	}
	return nil
}
func (e *LoginRequest) UnMarshallBody(d *http.Request) error {
	reqBody, _ := ioutil.ReadAll(d.Body)
	return json.Unmarshal(reqBody, e)
}

func (e *LoginRequest) MarshalIntoBody() ([]byte, error) {
	return json.Marshal(e)
}

type LoginResponse struct {
	AuthToken string `json:"auth_token"`
}
