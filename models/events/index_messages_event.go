package event

import (
	"acaisoftProject/models/schema"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type IndexMessagesRequest struct {
	Email string `json:"email"`
	Test  bool   `json:"test"`
}

func (e *IndexMessagesRequest) GetEntityFrom(d *http.Request) error {
	if err := e.UnMarshallBody(d); err != nil {
		return err
	}
	return nil
}
func (e *IndexMessagesRequest) UnMarshallBody(d *http.Request) error {
	reqBody, _ := ioutil.ReadAll(d.Body)
	return json.Unmarshal(reqBody, e)
}

func (e *IndexMessagesRequest) MarshalIntoBody() ([]byte, error) {
	return json.Marshal(e)
}

type IndexMessagesResponse struct {
	Node    []*schema.Messages
	HasNext bool
}
