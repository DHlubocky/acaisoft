package event

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type AddMessageRequest struct {
	Email   string `json:"email"`
	Content string `json:"content"`
	Title   string `json:"title"`
	Test    bool   `json:"test"`
}

func (e *AddMessageRequest) GetEntityFrom(d *http.Request) error {
	if err := e.UnMarshallBody(d); err != nil {
		return err
	}
	return nil
}
func (e *AddMessageRequest) UnMarshallBody(d *http.Request) error {
	reqBody, _ := ioutil.ReadAll(d.Body)
	return json.Unmarshal(reqBody, e)
}

func (e *AddMessageRequest) MarshalIntoBody() ([]byte, error) {
	return json.Marshal(e)
}

type AddMessageResponse struct {
	Completed bool
	Code      string
}
