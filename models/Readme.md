# Acaisoft models

The models folder and sub packages are used for storing models used in operations such as communication and operations
on the database

## Structure

**events**

The purpose of this sub folder is to store models used in communication through the web server

**schema**

The purpose of this sub folder is to store models used in persistent operations, including database operations such as
table models and filters