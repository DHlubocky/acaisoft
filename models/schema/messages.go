package schema

import "github.com/jinzhu/gorm"

type Messages struct {
	gorm.Model
	Code    string `gorm:"Column:Code"`
	Title   string `gorm:"Column:Title"`
	Email   string `gorm:"Column:Email"`
	Content string `gorm:"Column:Content"`
}

func (Messages) TableName() string {
	return "Messages"
}
