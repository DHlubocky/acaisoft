package schema

import (
	"acaisoftProject/repository/storage"
	"fmt"
	"github.com/jinzhu/gorm"
)

type UserFilter struct {
	storage.BaseFilter
	Username string
}

func (f *UserFilter) Apply(dbIn *gorm.DB) (dbOut *gorm.DB, err error) {
	f.Init()
	var applied bool
	dbOut, err = f.ApplyBase(dbIn)
	if err != nil {
		return
	}
	if len(f.Username) > 0 {
		applied = true
		dbOut = dbOut.Where(f.QueryString("Username", "="), f.Username)
	}
	if !applied {
		err = fmt.Errorf(`no parameter for filtering`)
		return
	}
	return
}

func (f *UserFilter) Init() {
	f.InitBase(true)
}
