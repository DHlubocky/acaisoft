package schema

import "github.com/jinzhu/gorm"

type Users struct {
	gorm.Model
	Code     string `gorm:"Column:Code"`
	Name     string `gorm:"Column:Name"`
	Username string `gorm:"Column:Username"`
	Password string `gorm:"Column:Password"`
}

func (Users) TableName() string {
	return "Users"
}
