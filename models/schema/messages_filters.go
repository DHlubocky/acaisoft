package schema

import (
	"acaisoftProject/repository/storage"
	"fmt"
	"github.com/jinzhu/gorm"
)

type MessageFilter struct {
	storage.BaseFilter
	Id   int
	Code string
}

func (f *MessageFilter) Apply(dbIn *gorm.DB) (dbOut *gorm.DB, err error) {
	f.Init()
	var applied bool
	dbOut, err = f.ApplyBase(dbIn)
	if err != nil {
		return
	}
	if f.Id != 0 {
		applied = true
		dbOut = dbOut.Where(f.QueryString("id", "="), f.Id)
	}
	if len(f.Code) < 0 {
		applied = true
		dbOut = dbOut.Where(f.QueryString("Code", "="), f.Code)
	}
	if !applied {
		err = fmt.Errorf(`no parameter for filtering`)
		return
	}
	return
}

func (f *MessageFilter) Init() {
	f.InitBase(true)
}

type MessageIndexFilter struct {
	storage.BaseIndexFilter
	Id    []int64
	Code  []string
	Email string
}

func (f *MessageIndexFilter) Init() {
	f.BaseIndexFilter.InitBase(true)
}

func (f *MessageIndexFilter) Apply(dbIn *gorm.DB) (dbOut *gorm.DB, err error) {
	f.Init()
	dbOut, _, err = f.ApplyBase(dbIn)
	if err != nil {
		return
	}

	if len(f.Id) > 0 {
		dbOut = dbOut.Where(f.QueryString("Id", "IN"), f.Id)
	}
	if len(f.Code) > 0 {
		dbOut = dbOut.Where(f.QueryString("Code", "IN"), f.Code)
	}
	if len(f.Email) > 0 {
		dbOut = dbOut.Where(f.QueryString("Email", "="), f.Email)
	}
	return
}

func (f *MessageIndexFilter) GetLimit() int {
	return f.BaseIndexFilter.Limit
}
